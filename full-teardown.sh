#!/bin/bash
# Exit on failure
set -ue

. config.sh

# Remove all resources created by terraform
terraform apply -auto-approve -destroy

# Now delete are resources that we had to create manually

# Delete the ecr repository we had to create outside terraform
# Also ignore failures
aws ecr delete-repository --repository-name quest --force 2>&1 || true
