# Use somewhat new nodejs version
FROM node:16

# First copy package json's over, this will allow us to cache the
# results of `npm install`, so we only run `npm install` when
# absolutely necessary
COPY package*.json ./

# Install packages from package.json
RUN npm install

# Copy everything else
COPY . .

EXPOSE 3000

ENTRYPOINT ["npm", "start"]
