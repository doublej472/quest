# README-JONATHANF

Hello! This is the readme file written by the applicant, Jonathan Frederick!

## Using this repo

If you want to deploy this on your own infrastructure, make sure you have AWS CLI v2, jq, Docker, and Terraform installed and configured, then run `full-deploy.sh`, which will build the docker image locally, setup the ACR repo, upload the docker image to the ACR repo, then run Terraform. The companion script, `full-teardown.sh` will do the opposite, where it destroys the Terraform environment, deletes the ACR repo, but it will not delete the image on your local machine.

I am treating ACR special because we are uploading a docker container to it. I could have managed the ACR repository in Terraform, but since I would need to leave Terraform right before uploading the container, then re-enter Terraform after to run the rest of the Terraform deployment, I decided to manage it entirely within bash. In a real environment, the container repository would probably be part of some CI/CD pipeline, as a separate step from deploying to production, so I think that is okay to do here.

## General architecture

I am packaging up the nodejs + Go application in docker, and uploading it to ACR. From there, the image is deployed to ECS, as a Fargate service (So I don't have to manage EC2 instances, but it is more tricky to setup). In front of this service, I have an ELB setup in IP mode, which is configured to listen on port 80 normally, and port 443 with a self-signed certificate stored in ACM. When ELB gets a connection attempt, it simply forwards it to the ECS Fargate containers over a private VPC.

# TODO (If I had more time...)

 - Use a proper DNS records instead of using the load balancer address, I think a CNAME from the desired DNS name to the LB endpoint should be good enough.
 - Use a real SSL certificate, if DNS records are managed in Route53 then ACM should work easily, otherwise maybe hook into LetsEncrypt? Validation would be non-trivial with LetsEncrypt, we would have to configure the load balancer to forward `/.well-known/acme-challenge` requests to a local validator, and then manually upload the certificate to ACM to be used by the load balancer.
 - Setup SSL certificate rotation, SSL certificates have an expiration date, so we need some automated way to renew certificates if they get close to expiry.
 - Remove public IP addresses from Fargate tasks, I had to add them to get Fargate to play nicely with ECR (It would fail to download the container), but it really shouldn't be necessary, since the load balancer connects over private IPs. Maybe setup a private endpoint for ECR in the VPC?
 - Use the latest platform version in Fargate, right now the application doesn't work 100% with version 1.4.0 (It can't detect that it's running in a container, probably because AWS changed to containerd in this release), so in Terraform I'm forcing 1.3.0.
 - **DONT HARDCODE THE SECRET_WORD ENV VARIABLE!** In a real production system this should have been put in parameter store or some other secrets management system.
 - Use more availability zones! right now I'm allocating the minimum number of availability zones required for the ECS service to deploy correctly (2), but if this is to scale out more, or be more resilient to AWS failures, we should allocate more.
 - Add observability! There is no logging being done right now, and this could really use some CloudWatch logging if this was to be deployed in production. There are some CloudWatch ECS metrics we can see by default, so at least we have that.
 - If this is important, set up some sort of uptime monitoring, and hook it in with something like PagerDuty, so it can be put into the on-call responsibilities.
 - Maybe instrument the container resource usage, so we can accurately size the ECS resource requests, along with hard and soft memory limits.
 - Maybe make the container port configurable in Terraform, it is used all over the place in the Terraform code.
 - Port the Go part of the application to ARM, because those instances are generally cheaper :)
