# You will need to manually update the subnets below if you change this
variable "region" {
  type    = string
  default = "us-west-2"
}

# You can change the number of containers created with
# this variable, if you prefer
variable "replicas" {
  type    = number
  default = 3
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.1"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {}

# START AWS NETWORKING CONFIGURATION

resource "aws_security_group" "allow_3000" {
  name        = "allow_3000"
  description = "Allow internal traffic to port 3000"
  vpc_id      = aws_vpc.quest_vpc.id

  ingress {
    description = "HTTP to port 3000"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.quest_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http(s)"
  description = "Allow HTTP(S) inbound traffic"
  vpc_id      = aws_vpc.quest_vpc.id

  ingress {
    description      = "HTTP from Public Internet"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "HTTPS from Public Internet"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

# Create a custom VPC so we don't depend on the default VPC
resource "aws_vpc" "quest_vpc" {
  cidr_block           = "10.4.0.0/16"
  enable_dns_hostnames = "true"
}

resource "aws_subnet" "quest_vpc_subnet_2a" {
  vpc_id            = aws_vpc.quest_vpc.id
  cidr_block        = "10.4.1.0/24"
  availability_zone = "us-west-2a"
}

resource "aws_subnet" "quest_vpc_subnet_2b" {
  vpc_id            = aws_vpc.quest_vpc.id
  cidr_block        = "10.4.2.0/24"
  availability_zone = "us-west-2b"
}

resource "aws_internet_gateway" "quest_gw" {
  vpc_id = aws_vpc.quest_vpc.id
}

# We need to manually setup the route table for the internet GW
# Also terraform treats this resource special, so make sure you
# know what you are doing before changing things here
resource "aws_default_route_table" "quest_rt" {
  default_route_table_id = aws_vpc.quest_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.quest_gw.id
  }
}

# START INSTANCE CONFIGURATION SECTION

# Allow the fargate containers to assume this role, which will then allow them to fetch containers
# (and write to logs, but I'm not setting up logging for this)
resource "aws_iam_role" "quest_execution_role" {
  name                = "questExecutionRole"
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"]
  assume_role_policy = jsonencode({
    "Version" : "2008-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ecs-tasks.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_ecs_cluster" "quest_cluster" {
  name = "quest-cluster"
}

resource "aws_ecs_task_definition" "quest_task_def" {
  family                   = "quest"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 512
  memory                   = 1024
  execution_role_arn       = aws_iam_role.quest_execution_role.arn
  container_definitions = jsonencode(
    [
      {
        "name" : "quest-container",
        "image" : "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com/quest:latest",
        "cpu" : 512,
        "memory" : 1024,
        "essential" : true,
        "environment" : [
          { "name" : "SECRET_WORD", "value" : "TwelveFactor" }
        ],
        "portMappings" = [
          {
            "containerPort" = 3000
            "hostPort"      = 3000
          }
        ]
      }
    ]
  )

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
}

resource "aws_ecs_service" "quest_service" {
  name            = "quest-service"
  cluster         = aws_ecs_cluster.quest_cluster.id
  task_definition = aws_ecs_task_definition.quest_task_def.arn
  desired_count   = var.replicas
  launch_type     = "FARGATE"
  # the application actually fails to detect if it's in a docker container on 1.4.0, I think
  # it has to do with aws changing from docker to containerd in between those 2 versions
  # In this case, force 1.3.0 because it plays nicer with the application
  platform_version = "1.3.0"
  network_configuration {
    subnets         = [aws_subnet.quest_vpc_subnet_2a.id, aws_subnet.quest_vpc_subnet_2b.id]
    security_groups = [aws_security_group.allow_3000.id]
    # This should really not be true
    assign_public_ip = "true"
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.quest_lb_target.arn
    container_name   = "quest-container"
    container_port   = 3000
  }
}

# START CERTIFICATE SETUP
resource "tls_private_key" "quest" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "quest_cert" {
  key_algorithm   = "RSA"
  private_key_pem = tls_private_key.quest.private_key_pem

  subject {
    common_name  = "*.elb.amazonaws.com"
    organization = "Quest Incorporated"
  }

  validity_period_hours = 12

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource "aws_acm_certificate" "quest_aws_cert" {
  private_key      = tls_private_key.quest.private_key_pem
  certificate_body = tls_self_signed_cert.quest_cert.cert_pem
}

# START LOAD BALANCE SECTION

resource "aws_lb_target_group" "quest_lb_target" {
  name        = "quest-lb-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.quest_vpc.id
}

resource "aws_lb" "quest_lb" {
  name               = "quest-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_http.id]
  subnets            = [aws_subnet.quest_vpc_subnet_2a.id, aws_subnet.quest_vpc_subnet_2b.id]
}

resource "aws_lb_listener" "quest_lb_listener" {
  load_balancer_arn = aws_lb.quest_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.quest_lb_target.arn
  }
}

resource "aws_lb_listener" "quest_lb_listener_ssl" {
  load_balancer_arn = aws_lb.quest_lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.quest_aws_cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.quest_lb_target.arn
  }
}
