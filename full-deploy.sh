#!/bin/bash
# Exit on failure
set -ue

# Get global envronment variables shared with the teardown
. config.sh

# Build the docker image locally, used by all platforms
docker build -t quest:latest .

# Create an ACR repo, and configure docker to use it
ACR_REPO_URI=$(aws ecr create-repository --repository-name quest | jq -r '.repository.repositoryUri')

# Configure docker so we can push the image we are building
aws ecr get-login-password --region "$AWS_REGION" | docker login --username AWS --password-stdin "$ACR_REPO_URI"

# Tag the locally built image to match AWS repo
docker tag quest:latest "$ACR_REPO_URI:latest"

# Finally push the docker container to AWS ECR
docker push "$ACR_REPO_URI:latest"

# Now we can start with terraform
terraform init
terraform apply -auto-approve

URL=$(terraform show -json | jq -r '.values.root_module.resources[] | select(.address == "aws_lb.quest_lb").values.dns_name')

echo "You can access your deployment at https://$URL once the health checks have passed"
echo "You may need to wait for up to 5 minutes"
